/*
 *
 * The following JSON template shows what is sent as the payload:
{
    "serialNumber": "GXXXXXXXXXXXXXXXXX",
    "batteryVoltage": "xxmV",
    "clickType": "SINGLE" | "DOUBLE" | "LONG"
}
 *
 * A "LONG" clickType is sent if the first press lasts longer than 1.5 seconds.
 * "SINGLE" and "DOUBLE" clickType payloads are sent for short clicks.
 *
 * For more documentation, follow the link below.
 * http://docs.aws.amazon.com/iot/latest/developerguide/iot-lambda-rule.html
 */

'use strict';

const request = require('request');
const secretKey = process.env.SECRET_KEY;
const hostname = process.env.HOSTNAME;
const powerSwitchIPPort = process.env.POWER_SWITCH_IP_PORT;

var invokeWithAuth = function(urlToInvoke, accessToken, callback) {
      request({url: urlToInvoke, method: 'GET', headers: {'Cookie': ('oauthpennidinh=' + accessToken)}}, function (error, response, body) {
         console.log('error:', error); // Print the error if one occurred
         console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
         console.log('body:', body); // Print the HTML for the Google homepage.
         callback();
      });
};

exports.handler = (event, context, callback) => {
    var powerSwitchIPAddress;
    var thermostatDeviceId;

    if (event.serialNumber == 'G030JF054162VUQ1') {
      powerSwitchIPAddress = '10.0.0.84'
      thermostatDeviceId = 'pennidinh-0';
    } else if (event.serialNumber == 'G030JF0570936411') {
      powerSwitchIPAddress = '10.0.0.162'
      thermostatDeviceId = 'pennidinh-1';
    } else if (event.serialNumber == 'G030JF0540412AST') {
      powerSwitchIPAddress = '10.0.0.181'
      thermostatDeviceId = 'pennidinh-2';
    } else {
      console.log('Recieved unexpected serial number: ' + event.serialNumber); 
      callback();
      return;
    }

    console.log('Received event:', event);

    console.log('fetching access token...');
    request('https://' + hostname + '/secure-node-js/getAccessToken.js?clientId=PenniDinh&appSecretKey=' + secretKey, function(error, response, body) {

      console.log('error: ' + error);
      console.log('response: ' + response);
      console.log('body: ' + body);
        
      var accessToken = JSON.parse(body).accessToken;
      console.log('accessToken = ' + accessToken);
      
      var urlToInvoke;
      if (event.clickType == 'SINGLE') {
            console.log('Received a single-click event. Turning thermostat on...');
            urlToInvoke = 'https://' + hostname + '/secure-node-js/setHomeHeater.json?powerSwitchIPAddress=' + powerSwitchIPAddress + '&powerSwitchIPPort=' + powerSwitchIPPort + '&thermosatDeviceID=' + thermostatDeviceId + '&newCurrentPower=true';
            console.log('making event call to ' + hostname + '...');
            invokeWithAuth(urlToInvoke, accessToken, callback);
      } else if (event.clickType == 'DOUBLE') {
            console.log('Received a double-click event. Turning thermostat off.');
            urlToInvoke = 'https://' + hostname + '/secure-node-js/setHomeHeater.json?powerSwitchIPAddress=' + powerSwitchIPAddress + '&powerSwitchIPPort=' + powerSwitchIPPort + '&thermosatDeviceID=' + thermostatDeviceId + '&newCurrentPower=false';
            console.log('making event call to ' + hostname + '...');
            invokeWithAuth(urlToInvoke, accessToken, callback);
      } else if (event.clickType == 'LONG') {
            console.log('Recieved a long-press event. Fetching current light status...');
            request({url: 'https://' + hostname + '/secure-node-js/getLights.json', method: 'GET', headers: {'Cookie': ('oauthpennidinh=' + accessToken)}}, function (error, response, body) {
              if (error) {
                console.log('error:', error); // Print the error if one occurred
                return;
              }
              if (!response || response.statusCode != 200) {
                console.log('Non-200 status code');
                console.log('statusCode:', response.statusCode); // Print the response status code if a response was received
                return;
              }
              console.log('body:', body); // Print the HTML for the Google homepage.

              var lightOn = false;
              var bodyJson = JSON.parse(body);
              Object.keys(bodyJson).forEach(function(key) {
                var value = bodyJson[key];
                if (value && value.state && value.state.on) {
                  lightOn = true;
                }
              });

              if (lightOn) {
                console.log('at least one light is on');
                urlToInvoke = 'https://' + hostname + '/secure-node-js/leaveTheHouse.json';
              } else {
                console.log('no lights are on');
                urlToInvoke = 'https://' + hostname + '/secure-node-js/arriveAtTheHouse.json';
              }

              invokeWithAuth(urlToInvoke, accessToken, callback);
            });
      } else {
            console.log('Recieved unsupported click type: ' + event.clickType);
            callback();
      }

    });
};
